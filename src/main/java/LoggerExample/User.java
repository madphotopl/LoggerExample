package LoggerExample;

import java.time.LocalDate;

public class User {


    //id, imię, nazwisko, login, hasło, e-mail oraz data urodzenia
    private int id;
    private String name;
    private String login;
    private String eMail;
    private int age;
    private String password;
    private LocalDate date;
    

    public String getLogin(){
        return login;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }


}
