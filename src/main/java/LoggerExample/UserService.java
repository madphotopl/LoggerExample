package LoggerExample;

public interface UserService {
   boolean registerUser (User user);
   boolean loginUser (String login, String password);
   boolean changePassword (String login, String oldPassword,String newPassword);



}
