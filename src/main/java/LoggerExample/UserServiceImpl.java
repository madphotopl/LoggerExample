package LoggerExample;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {
    private List<User> userList = new ArrayList<>();

    @Override
    public boolean registerUser(User user) {
        // userList.add(user);
        for (int i = 0; i < userList.size(); i++) {
            User userFromDb = userList.get(i);
            if (userFromDb.getLogin().equals(user.getLogin())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean loginUser(String login, String password) {
        for (int i = 0; i < userList.size(); i++) {
            User userFromDb = userList.get(i);
            if (userFromDb.getLogin().equals(login)) {

                if (userFromDb.getPassword().equals(password)) {
                    return true;
                }
            }


        }
        return false;
    }

    @Override
    public boolean changePassword(String login, String oldPassword,String newPassword) {
        for (int i = 0; i < userList.size(); i++) {
            User userFromList = userList.get(i);
            if (userFromList.getLogin().equals(login)) {

                if (userFromList.getPassword().equals(oldPassword)) {
                   userFromList.setPassword(newPassword);
                }


            }

        }
        return false;
    }

}